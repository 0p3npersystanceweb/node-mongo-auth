# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* NodeJS node-mongo auth rest api
* Version 1.0


### Steps to Run docker image ###

* docker build -t node-mongo-auth .
* docker run -it -p 9000:3000 node-mongo-auth


### Steps to run test cases ###

* node run test
